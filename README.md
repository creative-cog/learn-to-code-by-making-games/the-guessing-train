# The Guessing Train
Episode 1 - Basic Training


## Getting started
Just open the index.html file in your web browser.
That's it :)

If you want to get fancy, install nodeJS on your computer, then run:

`
npm install -g lite-server
`

This is a development-only easy to use web-server

In a terminal program (depending on your OS), go to the folder containing your `index.html` file, then run:

`lite-server`

The page should automatically load in your browser over localhost
